#include "voretournament.qh"
#include "voretournament.qc"

#ifdef EFFECTINFO_ENABLED
	#include "../common/macro/effectinfo.qh"
#endif
#include "../common/macro/macro_defs.qh"
#include "macro/macro_main.qh"
#include "macro/macro_main.qc"
#include "macro/macro_bot.qh"
#include "macro/macro_bot.qc"
#include "macro/macro.qc"

#ifdef EFFECTINFO_ENABLED
	#include "../common/vore/effectinfo.qh"
#endif
#include "../common/vore/vore_defs.qh"
#include "vore/vore_main.qh"
#include "vore/vore_main.qc"
#include "vore/vore_bot.qh"
#include "vore/vore_bot.qc"
#include "vore/vore.qc"
