#!/usr/bin/env bash

os=$(uname -o)
arch=$(uname -m)

if [ "${os}" == "Msys" ] ; then
	if [ "${arch}" == "x86_64" ] ; then
		pacman --needed -S git curl zip unzip p7zip make automake autoconf libtool gcc gmp-devel mingw-w64-x86_64-{toolchain,make,gcc,gmp,SDL2,libjpeg-turbo,libpng,libogg}
	else
		pacman --needed -S git curl zip unzip p7zip make automake autoconf libtool gcc gmp-devel mingw-w64-i686-{toolchain,make,gcc,gmp,SDL2,libjpeg-turbo,libpng,libogg}
	fi
fi
